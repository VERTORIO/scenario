require 'lib/event_extend'

Event.register(Event.core_events.init, function()
    for _, force in pairs(game.forces) do
        force.technologies['atomic-bomb'].enabled = false
    end
end)
