# VERTORIO Softmods Collection 1

## About this collection

This is a collection of Factorio softmods (sometimes with other origins) for the VERTORIO servers.  The starting goal is to take ideal functionality from several other softmods, and make it possible to have it run properly in a single server.  Later goals are TBD.

This pack implements these mods:
- Autodeconstruct (marks miners with no resources for deconstruction);
- Autofill (with option for player to turn it off for them);
- Band (an evolved player "Tag");
- Starting-items (based on the tech tree progression players will spawn with better items);
- The entire slew of tools from FishBus Gaming, including: Player list, server information, admin & advanced admin tools, anti-griefing, and an automatic repeating message.
