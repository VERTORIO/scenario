-- Used by ktags.lua
-- License: Public Domain

-- Feel free to edit.

return {

["roles"] = {
	["Military"] = {
		i18n_name = 'Military',
		icons = {
			"item/submachine-gun",
		}
	},
		
	["Mining / Outposts"] = {
		i18n_name = 'Mining / Outposts',
		icons = {
			"item/electric-mining-drill",
		}
	},
	
	["Smelting"] = {
		i18n_name = 'Smelting',
		icons = {
			"item/electric-furnace",
		}
	},
	
	["Production"] = {
		i18n_name = 'Production',
		icons = {
			"item/assembling-machine-3",
		}
	},
	
	["Science"] = {
		i18n_name = 'Science',
		icons = {
			"item/space-science-pack",
		}
	},

	["Circuits"] = {
		i18n_name = 'Circuits',
		icons = {
			"item/red-wire",
		}
	},
	
	["Trains"] = {
		i18n_name = 'Trains',
		icons = {
			"item/locomotive",
		}
	},

	["Oil"] = {
		i18n_name = 'Oil',
		icons = {
			"fluid/crude-oil",
		}
	},
	
	["Power"] = {
		i18n_name = 'Power',
		icons = {
			"item/nuclear-reactor",
		}
	},

	["Walls / Defense"] = {
		i18n_name = 'Walls / Defense',
		icons = {
			"item/stone-wall",
		}
	},
	
	["Optimisation"] = {
		i18n_name = 'Optimisation',
		icons = {
			"item/iron-gear-wheel",
		}
	},

	["Quality Control"] = {
		i18n_name = 'Quality Control',
		icons = {
			"item/repair-pack",
		}
	},

	["Foresting"] = {
		i18n_name = 'Foresting',
		icons = {
			"item/raw-wood",
		}
	},
	
}, -- end of roles

}