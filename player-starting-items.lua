-- Helpers
if MODULE_LIST then
	module_list_add("Player Starting Items")
end

local function ticks_from_minutes(minutes)
	return minutes * 60 * 60
end

-- Give player items when logging in or respawning based on progression
function player_spawned(event)
	
	local player = game.players[event.player_index]

	-- give weapon
	if (player.force.technologies["military"].researched) then
		player.insert { name = "submachine-gun", count = 1 }
	else
		player.insert { name = "pistol", count = 1 }
	end

	-- give ammo
	if (player.force.technologies["uranium-ammo"].researched) then
		player.insert { name = "uranium-rounds-magazine", count = 10 }
	else 
		if (player.force.technologies["military-2"].researched) then
			player.insert { name = "piercing-rounds-magazine", count = 10 }
		else
			player.insert { name = "firearm-magazine", count = 10 }
		end
	end

	if game.tick > ticks_from_minutes(20) then
		-- give armor
		if (player.force.technologies["modular-armor"].researched) then		
			-- modular armors
			if (player.force.technologies["power-armor-2"].researched) then
				player.insert { name = "power-armor-mk2", count = 1 }
				-- give equipment
				if player.character.grid then
					player.character.grid.put({name="fusion-reactor-equipment", position = {0,0} })

					player.character.grid.put({name="battery-mk2-equipment", position = {0,4} })				    
					player.character.grid.put({name="battery-mk2-equipment", position = {1,4} })

					player.character.grid.put({name="personal-roboport-mk2-equipment", position = {2,4} })

					player.character.grid.put({name="exoskeleton-equipment", position = {0,6} })				    
					player.character.grid.put({name="exoskeleton-equipment", position = {2,6} })
				end
			elseif (player.force.technologies["power-armor"].researched) then
				player.insert { name = "power-armor", count = 1 }
				-- give equipment
				if player.character.grid then
					player.character.grid.put({name="fusion-reactor-equipment", position = {0,0} })

					player.character.grid.put({name="battery-equipment", position = {0,4} })				    
					player.character.grid.put({name="battery-equipment", position = {1,4} })

					player.character.grid.put({name="personal-roboport-equipment", position = {2,4} })

					player.character.grid.put({name="exoskeleton-equipment", position = {5,3} })
				end
			else
				player.insert { name = "modular-armor", count = 1 }
				-- give equipment
				if player.character.grid then

					player.character.grid.put({name="solar-panel-equipment", position = {0,0} })
					player.character.grid.put({name="solar-panel-equipment", position = {1,0} })
					player.character.grid.put({name="solar-panel-equipment", position = {2,0} })
					player.character.grid.put({name="solar-panel-equipment", position = {3,0} })
					player.character.grid.put({name="solar-panel-equipment", position = {4,0} })

					player.character.grid.put({name="solar-panel-equipment", position = {0,1} })
					player.character.grid.put({name="solar-panel-equipment", position = {1,1} })
					player.character.grid.put({name="solar-panel-equipment", position = {2,1} })
					player.character.grid.put({name="solar-panel-equipment", position = {3,1} })
					player.character.grid.put({name="solar-panel-equipment", position = {4,1} })

					player.character.grid.put({name="solar-panel-equipment", position = {0,2} })
					player.character.grid.put({name="solar-panel-equipment", position = {1,2} })
					player.character.grid.put({name="solar-panel-equipment", position = {2,2} })
					player.character.grid.put({name="solar-panel-equipment", position = {3,2} })
					player.character.grid.put({name="solar-panel-equipment", position = {4,2} })

					player.character.grid.put({name="battery-equipment", position = {0,3} })				    
					player.character.grid.put({name="battery-equipment", position = {1,3} })
					player.character.grid.put({name="battery-equipment", position = {2,3} })

					player.character.grid.put({name="personal-roboport-equipment", position = {3,3} })
				end
			end
		elseif (player.force.technologies["heavy-armor"].researched) then
			player.insert { name = "heavy-armor", count = 1 }
		else
			player.insert { name = "light-armor", count = 1 }
		end
	end

	-- give starting items
	if game.tick < ticks_from_minutes(10) then
		player.insert { name = "burner-mining-drill", count = 2 }
		player.insert { name = "stone-furnace", count = 2 }
	end

	-- give tools
	if (player.force.technologies["steel-processing"].researched) then
		player.insert { name = "steel-axe", count = 2 }
	else
		player.insert { name = "iron-axe", count = 5 }
	end    

	-- set spawn position
	player.force.set_spawn_position({0,0}, player.surface)
end

Event.register(defines.events.on_player_created, player_spawned)
Event.register(defines.events.on_player_respawned, player_spawned)